export DOTFILES_HOME="$HOME/.dotfiles"

# xdg user directories
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# zsh
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"


# misc
export BROWSER="firefox"

export EDITOR="nvim"
export VISUAL="nvim"

export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANROFFOPT="-c"
export PAGER="less -RF"


# kubectl
export KUBECONFIG="$XDG_CONFIG_HOME/kubectl.yaml"

# rust
export RUSTC_WRAPPER="sccache"
