#!/usr/bin/env bash
set -e

function link() {
	local src=$1
	local dest=$2

	if [[ -L "$dest" ]] && [[ $(readlink "$dest") == "$src" ]]; then
		return 0
	fi
	if [[ -e "$dest" ]]; then
		mv "$dest" "$dest.bak"
	fi

	echo ln -sf "$src" "$dest"
	ln -sf "$src" "$dest"
}

function link_all() {
	local src_dir=$1
	local dest_dir=$2

	echo "# $src_dir -> $dest_dir"
	mkdir -p "$src_dir" "$dest_dir"

	find "$src_dir" -mindepth 1 -maxdepth 1 -print |\
		sort -u |\
		while read -r file; do
			local src_rel
			src_rel=$(realpath --relative-to="$src_dir" "$file")

			link "$file" "$dest_dir/$src_rel"
		done
}

dotdir=${1:-$HOME/.dotfiles}
link_all "$dotdir/home" "$HOME"
link_all "$dotdir/config" "${XDG_CONFIG_HOME:-$HOME/.config}"
