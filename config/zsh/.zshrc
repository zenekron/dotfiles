# vim: ft=zsh

source "/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
source "/usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh"
source "/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
source "/usr/share/nvm/init-nvm.sh"
eval "$(direnv hook zsh)"
eval "$(starship init zsh)"

source "$ZDOTDIR/aliases.zsh"


# options
setopt HIST_IGNORE_DUPS
setopt SHARE_HISTORY


# pnpm
export PNPM_HOME="$XDG_DATA_HOME/pnpm"

# ssh agent
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
	ssh-agent -t 1h > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if [[ ! -f "$SSH_AUTH_SOCK" ]]; then
	source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
fi


# path - https://zsh.sourceforge.io/Guide/zshguide02.html#l24
typeset -U path
path=(
	$HOME/.dotfiles/bin
	$HOME/.cargo/bin
	$PNPM_HOME
	$path
)


# completion
autoload -Uz compinit; compinit
zstyle ':completion:*' menu select # navigate completion with arrows

# zsh history
export HISTFILE="$XDG_CACHE_HOME/zhistory"
export HISTSIZE="1000"
export SAVEHIST="1000"

# zplug
source $DOTFILES_HOME/modules/zplug/init.zsh
zplug "b4b4r07/enhancd", use:init.sh
zplug "chisui/zsh-nix-shell", use:nix-shell.plugin.zsh
zplug load

# keybinds
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
