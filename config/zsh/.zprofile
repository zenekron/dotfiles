export WLR_NO_HARDWARE_CURSORS=1
export _JAVA_AWT_WM_NONREPARENTING=1
# Firefox - https://wiki.archlinux.org/title/Firefox#Wayland
export MOZ_ENABLE_WAYLAND=1

if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
	exec sway
fi
