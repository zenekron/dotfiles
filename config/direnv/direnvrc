#!/usr/bin/env bash

# use java <version> [path]
use_java() {
	local java_version="$1"
	if [[ -z "$java_version" ]]; then
		log_error "Missing Java version"
		exit 1
	fi

	local java_prefix="${2:-/usr/lib/jvm}"
	if [[ ! -d "$java_prefix" ]]; then
		log_error "Invalid Java prefix"
		exit 1
	fi

	# select version
	java_version="$(find "$java_prefix" -mindepth 1 -maxdepth 1 -type d -iname "*-${java_version}-*" | head -n 1)"
	if [[ -z "$java_version" ]] || [[ ! -d "$java_version" ]]; then
		log_error "No matching version found for input: $java_version"
		exit 1
	fi

	# switch to version
	export JAVA_HOME="$java_version"
	PATH_add "$JAVA_HOME/bin"
	PATH_add "$JAVA_HOME/jre/bin"

    log_status "Successfully loaded Java $(java -version 2>&1 | head -n 1)"
}

use_nvm() {
	local node_version=$1

	nvm_sh="$HOME/.nvm/nvm.sh"
	if [[ -e "$nvm_sh" ]]; then
		source "$nvm_sh"
		nvm use "$node_version"
	fi
}

layout_poetry() {
	if [[ ! -f pyproject.toml ]]; then
		log_error "No pyproject.toml found. Use \`poetry new\` or \`poetry init\` to create one first."
		exit 2
	fi

	# create venv if it doesn't exist
	poetry run true

	VIRTUAL_ENV="$(poetry env info --path)"
	export VIRTUAL_ENV
	export POETRY_ACTIVE=1
	PATH_add "$VIRTUAL_ENV/bin"
}
