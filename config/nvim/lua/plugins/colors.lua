return {
  -- An arctic, north-bluish clean and elegant Vim theme.
  -- https://github.com/arcticicestudio/nord-vim
  "arcticicestudio/nord-vim",

  -- One dark and light colorscheme for neovim >= 0.5.0
  -- https://github.com/navarasu/onedark.nvim
  {
    "navarasu/onedark.nvim",
    opts = {
      style = "dark",
    },
  },

  -- A dark Vim/Neovim color scheme inspired by Atom's One Dark syntax theme.
  -- https://github.com/joshdick/onedark.vim
  { "joshdick/onedark.vim", enabled = false },

  -- NeoVim dark colorscheme inspired by the colors of the famous painting by Katsushika Hokusai.
  -- https://github.com/rebelot/kanagawa.nvim
  "rebelot/kanagawa.nvim",

  -- High Contrast & Vivid Color Scheme based on Monokai Pro
  -- https://github.com/sainnhe/sonokai
  "sainnhe/sonokai",

  -- Soothing pastel theme for (Neo)vim
  -- https://github.com/catppuccin/nvim
  { "catppuccin/nvim",      name = "catppuccin" },
}
