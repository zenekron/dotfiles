local function noop()
end

return {
  -- Snippet Engine for Neovim written in Lua.
  -- https://github.com/L3MON4D3/LuaSnip
  {
    "L3MON4D3/LuaSnip",
    dependencies = {
      -- Snippets collection for a set of different programming languages for faster development.
      -- https://github.com/rafamadriz/friendly-snippets
      "rafamadriz/friendly-snippets",
    },
    build = "make install_jsregexp", -- install jsregexp (optional)
    config = function()
      require("luasnip.loaders.from_snipmate").lazy_load()
      require("luasnip.loaders.from_vscode").lazy_load()

      vim.api.nvim_create_user_command("LuaSnipEdit", function()
        require("luasnip.loaders").edit_snippet_files({})
      end, {})
    end
  },

  -- A completion engine plugin for neovim written in Lua.
  -- https://github.com/hrsh7th/nvim-cmp
  {
    "hrsh7th/nvim-cmp",
    dependencies = {
      -- nvim-cmp source for buffer words.
      -- https://github.com/hrsh7th/cmp-buffer
      "hrsh7th/cmp-buffer",

      -- nvim-cmp source for neovim's built-in language server client.
      -- https://github.com/hrsh7th/cmp-nvim-lsp
      "hrsh7th/cmp-nvim-lsp",

      -- nvim-cmp source for displaying function signatures with the current parameter emphasized
      -- https://github.com/hrsh7th/cmp-nvim-lsp-signature-help
      "hrsh7th/cmp-nvim-lsp-signature-help",

      -- nvim-cmp source for filesystem paths.
      -- https://github.com/hrsh7th/cmp-path
      "hrsh7th/cmp-path",

      -- luasnip completion source for nvim-cmp
      -- https://github.com/saadparwaiz1/cmp_luasnip
      { "saadparwaiz1/cmp_luasnip" },
    },
    opts = function()
      local cmp = require("cmp")

      local expand = noop
      local supertab = {
        next = { can = noop, exec = noop },
        prev = { can = noop, exec = noop },
      }

      local has_luasnip, luasnip = pcall(require, "luasnip")
      if has_luasnip then
        expand = function(args)
          return luasnip.lsp_expand(args.body)
        end

        supertab.next.can = luasnip.expand_or_jumpable
        supertab.next.exec = luasnip.expand_or_jump
        supertab.prev.can = function()
          return luasnip.jumpable(-1)
        end
        supertab.prev.exec = function()
          return luasnip.jump(-1)
        end
      else
        vim.notify("[nvim-cmp] no snippet engine configured", vim.log.levels.TRACE)
      end

      return {
        snippet = { expand = expand },
        window = {
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },
        mapping = cmp.mapping.preset.insert({
          ["<CR>"] = cmp.mapping.confirm({ select = true }),
          ["<C-Space>"] = cmp.mapping.complete({}),
          ["<C-e>"] = cmp.mapping.abort(),
          ["<C-f>"] = cmp.mapping.scroll_docs(4),
          ["<C-d>"] = cmp.mapping.scroll_docs(-4),
          ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_next_item()
            elseif supertab.next.can() then
              supertab.next.exec()
            else
              fallback()
            end
          end, { "i", "s" }),
          ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_prev_item()
            elseif supertab.prev.can() then
              supertab.prev.exec()
            else
              fallback()
            end
          end, { "i", "s" }),
        }),
        cmp.setup({
          sources = cmp.config.sources(
            {
              { name = "nvim_lsp" },
              { name = "nvim_lsp_signature_help" },
              { name = "luasnip" },
              { name = "path" },
            },
            {
              { name = "buffer" },
            }
          ),
        })
      }
    end,
  },
}
