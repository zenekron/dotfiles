return {
  -- Quickstart configs for Nvim LSP
  -- https://github.com/neovim/nvim-lspconfig
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      -- JSON schemas for Neovim
      -- https://github.com/b0o/SchemaStore.nvim
      "b0o/schemastore.nvim",

      -- Lua LSP configuration for neovim lua development.
      -- https://github.com/folke/neodev.nvim
      { "folke/neodev.nvim",   config = true },

      -- nvim-cmp source for neovim's built-in language server client.
      -- https://github.com/hrsh7th/cmp-nvim-lsp
      { "hrsh7th/cmp-nvim-lsp" },
    },
    config = function(_, opts)
      local lspconfig = require("lspconfig")

      local has_nvim_cmp = pcall(require, "nvim-cmp")
      if has_nvim_cmp then
        local winopts = require("cmp.config.window").bordered()

        lspconfig.util.default_config = vim.tbl_extend(
          "force",
          lspconfig.util.default_config,
          {
            capabilities = require("cmp_nvim_lsp").default_capabilities(),
            handlers = {
              ["textDocument/hover"]         = vim.lsp.with(vim.lsp.handlers["textDocument/hover"], winopts),
              ["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers["textDocument/signatureHelp"], winopts),
              hover                          = vim.lsp.with(vim.lsp.handlers.hover, winopts),
              signature_help                 = vim.lsp.with(vim.lsp.handlers.signature_help, winopts),
            },
          }
        )
      end

      for server_name, server_opts in pairs(opts.servers) do
        lspconfig[server_name].setup(server_opts)
      end

      --
      -- Keys
      --

      local kopts = { noremap = true, silent = true }

      -- diagnostics
      vim.keymap.set("n", "<leader>d", vim.diagnostic.setloclist)
      vim.keymap.set("n", "<leader>D", vim.diagnostic.open_float, kopts)
      vim.keymap.set("n", "[g", vim.diagnostic.goto_prev, kopts)
      vim.keymap.set("n", "]g", vim.diagnostic.goto_next, kopts)

      -- docs/info
      vim.keymap.set("n", "K", vim.lsp.buf.hover, kopts)
      vim.keymap.set("n", "<leader>K", vim.lsp.buf.signature_help, kopts)

      -- goto
      -- vim.keymap.set("n", "gd", vim.lsp.buf.definition, kopts)
      vim.keymap.set("n", "gD", vim.lsp.buf.declaration, kopts)
      -- vim.keymap.set("n", "gy", vim.lsp.buf.type_definition, kopts)
      -- vim.keymap.set("n", "gr", vim.lsp.buf.references, kopts)
      -- vim.keymap.set("n", "gi", vim.lsp.buf.implementation, kopts)

      -- actions
      vim.keymap.set("n", "<leader>ac", vim.lsp.buf.code_action, kopts)
      vim.keymap.set("x", "<leader>ac", vim.lsp.buf.code_action, kopts)
      vim.keymap.set("n", "<leader>f", vim.lsp.buf.format, kopts)
      vim.keymap.set("x", "<leader>f", vim.lsp.buf.format, kopts)
      vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, kopts)

      -- workspace
      vim.keymap.set("n", "<leader>wa", vim.lsp.buf.add_workspace_folder, kopts)
      vim.keymap.set("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder, kopts)
      vim.keymap.set("n", "<leader>wl", function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
      end, kopts)

      -- symbols
      vim.keymap.set("n", "<leader>o", vim.lsp.buf.document_symbol, kopts)
      vim.keymap.set("n", "<leader>s", vim.lsp.buf.workspace_symbol, kopts)
    end,
    opts = function()
      local util = require("lspconfig.util")
      local schemastore = require("schemastore")

      local docker_compose_language_service = {
        root_dir = util.root_pattern("docker-compose.yaml", "compose.yaml")
      }

      return {
        servers = {
          ansiblels = {},
          bashls = {},
          cssls = {},
          docker_compose_language_service = docker_compose_language_service,
          elmls = {},
          emmet_ls = {},
          graphql = {},
          html = {},
          nixd = {},
          pyright = {},
          svelte = {},
          terraformls = {},
          tsserver = {},

          jsonls = {
            settings = {
              json = {
                schemas = schemastore.json.schemas(),
                validate = { enable = true },
              },
            },
          },
          lua_ls = {
            root_dir = util.root_pattern("lazy-lock.json", ".luarc.json", ".luarc.jsonc", ".luacheckrc", ".stylua.toml",
              "stylua.toml", "selene.toml", "selene.yml", ".git")
          },
        },
      }
    end
  },



  --
  -- Language specific plugins
  --

  -- Clangd's off-spec features for neovim's LSP client
  -- https://sr.ht/~p00f/clangd_extensions.nvim
  {
    "https://git.sr.ht/~p00f/clangd_extensions.nvim",
    opts = {
      server = {
        capabilities = {
          offsetEncoding = { "utf-16" },
        },
      },
    },
  },

  -- A plugin to improve your rust experience in neovim.
  -- https://github.com/simrat39/rust-tools.nvim
  {
    "simrat39/rust-tools.nvim",
    config = true,
  },



  --
  -- Other
  --

  -- Use Neovim as a language server to inject LSP diagnostics, code actions, and more via Lua.
  -- https://github.com/jose-elias-alvarez/null-ls.nvim
  {
    "jose-elias-alvarez/null-ls.nvim",
    dependencies = {
      -- All the lua functions I don't want to write twice.
      -- https://github.com/nvim-lua/plenary.nvim
      "nvim-lua/plenary.nvim",
    },
    opts = function()
      local null_ls = require("null-ls")

      local sqlfluff_postgres = {
        extra_args = { "--dialect", "postgres" },
        filetypes = { "pgsql" },
      }

      return {
        sources = {
          --
          -- BUILTINS
          --

          -- Code Actions
          null_ls.builtins.code_actions.eslint,

          -- Formatting
          null_ls.builtins.formatting.black,
          null_ls.builtins.formatting.clang_format.with {
            filetypes = { "c", "cs", "cpp", "cuda" },
          },
          null_ls.builtins.formatting.nixpkgs_fmt,
          null_ls.builtins.formatting.prettier.with {
            filetypes = {
              "css",
              "html",
              "java",
              "javascript",
              "javascriptreact",
              "json",
              "jsonc",
              "markdown",
              "scss",
              "typescript",
              "typescriptreact",
              "xml",
              "yaml",
            },
          },
          null_ls.builtins.formatting.sqlfluff.with(sqlfluff_postgres),

          -- Diagnostics
          null_ls.builtins.diagnostics.checkmake,
          null_ls.builtins.diagnostics.cmake_lint,
          null_ls.builtins.diagnostics.cppcheck,
          null_ls.builtins.diagnostics.eslint,
          null_ls.builtins.diagnostics.shellcheck,
          null_ls.builtins.diagnostics.sqlfluff.with(sqlfluff_postgres),
          null_ls.builtins.diagnostics.zsh,
        },
      }
    end
  },

  -- A pretty list for showing diagnostics, references, telescope results, quickfix and location lists to help you solve all the trouble your code is causing.
  -- https://github.com/folke/trouble.nvim
  {
    "folke/trouble.nvim",
    dependencies = {
      -- Adds file type icons to Vim
      -- https://github.com/nvim-tree/nvim-web-devicons
      "nvim-tree/nvim-web-devicons",
    },
    keys = {
      { "<leader>d",  "<cmd>TroubleToggle<cr>" },
      { "<leader>tw", "<cmd>Trouble workspace_diagnostics<cr>" },
      { "<leader>td", "<cmd>Trouble document_diagnostics<cr>" },
      { "gd",         "<cmd>Trouble lsp_definitions<cr>" },
      { "gy",         "<cmd>Trouble lsp_type_definitions<cr>" },
      { "gr",         "<cmd>Trouble lsp_references<cr>" },
      { "gi",         "<cmd>Trouble lsp_implementations<cr>" },
    },
    config = true
  },
}
