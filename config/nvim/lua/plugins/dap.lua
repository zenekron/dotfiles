return {
  -- Debug Adapter Protocol client implementation for Neovim
  -- https://github.com/mfussenegger/nvim-dap
  {
    "mfussenegger/nvim-dap",
    keys = {
      {
        "<leader>dc",
        function()
          require("dap").continue()
        end,
      },
      {
        "<leader>dn",
        function()
          require("dap").step_over()
        end,
      },
      {
        "<leader>di",
        function()
          require("dap").step_into()
        end,
      },
      {
        "<leader>do",
        function()
          require("dap").step_out()
        end,
      },
      {
        "<leader>db",
        function()
          require("dap").toggle_breakpoint()
        end
      },
      {
        "<leader>dr",
        function()
          require("dap").repl.open()
        end,
      },
      {
        "<leader>dl",
        function()
          require("dap").run_last()
        end,
      },
    },
    config = function()
      local dap = require("dap")

      dap.adapters.lldb = {
        type = "executable",
        command = "lldb-vscode",
        name = "lldb",
      }

      dap.configurations.cpp = {
        {
          name = "Launch",
          type = "lldb",
          request = "launch",
          program = function()
            return vim.fn.input({
              prompt = "Path to executable: ",
              default = vim.fn.getcwd() .. "/",
              completion = "file",
            })
          end,
          cwd = "${workspaceFolder}",
          stopOnEntry = false,
          args = {},
          -- 💀
          -- if you change `runInTerminal` to true, you might need to change the yama/ptrace_scope setting:
          --
          --    echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
          --
          -- Otherwise you might get the following error:
          --
          --    Error on launch: Failed to attach to the target process
          --
          -- But you should be aware of the implications:
          -- https://www.kernel.org/doc/html/latest/admin-guide/LSM/Yama.html
          -- runInTerminal = false,
        },
      }
    end,
  },

  {
    -- A UI for nvim-dap
    -- https://github.com/rcarriga/nvim-dap-ui
    "rcarriga/nvim-dap-ui",
    dependencies = {
      -- Debug Adapter Protocol client implementation for Neovim
      -- https://github.com/mfussenegger/nvim-dap
      "mfussenegger/nvim-dap",
    },
    config = function(_, opts)
      local dap = require("dap")
      local dapui = require("dapui")

      require("dapui").setup(opts)

      dap.listeners.after.event_initialized["dapui_config"] = dapui.open
      dap.listeners.before.event_exited["dapui_config"] = dapui.close
      dap.listeners.before.event_terminated["dapui_config"] = dapui.close
    end,
    opts = {},
  },
}
