#!/usr/bin/env bash
set -euo pipefail

declare -a packages

packages+=(
	# git
	git
	git-cliff
	git-delta
	git-lfs

	# neovim
	neovim
	python-pynvim

	# cli tools
	ctags
	hyperfine
	lldb
	mitmproxy
	mkcert
	mold
	nmap
	sccache
	valgrind

	# gui tools
	code
	datagrip
	ghidra
	insomnia-bin # rest/graphql client

	# --------------------| Platforms |-------------------- #

	# docker
	docker
	docker-buildx
	docker-compose

	# kubernetes
	helm
	k9s
	kubectl


	# --------------------| Languages |-------------------- #

	# ansible
	ansible
	ansible-lint

	# c/c++
	checkmake
	cmake
	cmake-format
	cppcheck
	include-what-you-use
	ninja
	python-cpplint

	# css
	vscode-css-languageserver

	# html
	vscode-html-languageserver

	# java
	gradle
	gradle-{doc,src}
	jdk{,8,11,17}-openjdk
	jdtls
	maven
	openjdk{,8,11,17}-{doc,src}

	# javascript/typescript
	nvm
	prettier
	typescript-language-server

	# json
	vscode-json-languageserver

	# lua
	lua-language-server

	# python
	pyright
	python-poetry

	# rust
	cargo-edit
	cargo-expand
	cargo-feature
	cargo-generate
	cargo-msrv
	cargo-outdated
	cargo-release
	cargo-tauri
	cargo-udeps
	cargo-update
	cargo-watch
	rust-analyzer
	rustup
	wasm-{bindgen,pack}

	# shell
	bash-language-server
	shellcheck

	# sql
	sqlfluff

	# svelte
	svelte-language-server

	# terraform
	terraform
	terraform-ls
	terragrunt
	tfsec-bin

	# tex/latex
	tectonic
	texinfo
	texlab

	# zig
	zig
)

(IFS=$'\n'; echo "${packages[*]}")
