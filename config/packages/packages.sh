#!/usr/bin/env bash
set -euo pipefail

declare -a packages

packages+=(
	# core
	base
	base-devel

	# core utils
	man-db
	man-pages
	openssh
	pacman-contrib
	paru-bin
	reflector
	sudo

	# filesystems
	nfs-utils
	ntfs-3g
	sshfs

	# shell
	starship # prompt
	zsh
	zsh-autosuggestions
	zsh-completions
	zsh-history-substring-search
	zsh-syntax-highlighting

	# cli
	bat
	bc
	btop
	direnv
	dust
	entr
	exa
	fd
	figlet
	findimagedupes
	fzf
	gallery-dl
	htop
	ipcalc
	jq
	nix
	p7zip
	patch
	renameutils
	ripgrep
	rsync
	sd
	tealdeer
	tmux
	tmuxinator
	wget
	which

	# misc
	ventoy-bin
)

if [[ "$(cat /etc/hostname)" == "fry" ]]; then
	packages+=(
		linux-firmware
		linux-lts
		linux-lts-docs
		linux-lts-headers
		sof-firmware

		open-vm-tools
		systemd-boot-pacman-hook
		xdg-user-dirs
		udisks2

		# video
		xf86-video-vmware
		xorg-xwayland

		# audio
		pipewire
		pipewire-alsa
		pipewire-pulse
		wireplumber

		# wayland
		qt{5,6}-wayland

		# graphical environment
		clipman
		grim # wayland screenshot utility
		slurp # wayland utility to select regions
		sway
		swaylock
		waybar
		wofi

		# file navigation and preview
		feh
		nemo
		nemo-share
		okular
		vlc

		# apps
		firefox
		lunacy-bin
		thunderbird
		wezterm
	)
fi

(IFS=$'\n'; echo "${packages[*]}")
